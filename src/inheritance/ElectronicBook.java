//Leon Hazurin
//1640570

package inheritance;

public class ElectronicBook extends Book {

	private int numberBytes;
	
	public ElectronicBook(String title, String author, int numberBytes) {
		super(title,author);
		this.numberBytes = numberBytes;
	}
	
	public String toString() {
		String baseStr = super.toString();
		return (baseStr + ", number of bytes: " + (numberBytes));
	}
	
}
