//Leon Hazurin
//1640570

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Book[] books = new Book[5];
books[0] = new Book("Gone with the Wind", "Margaret Mitchell");
books[1] = new ElectronicBook("Pride and Prejudice", "Jane Austen", 20);
books[2] = new Book("The Raven", "Edgar Allan Poe");
books[3] = new ElectronicBook("Les miserables", "Victor Hugo", 65);
books[4] = new ElectronicBook("Sense and Sensibility", "Jane Austen", 89);
	
	for(int i = 0; i < books.length; i++) {
		System.out.println(books[i]);
	}
	}

}
