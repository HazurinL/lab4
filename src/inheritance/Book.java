//Leon Hazurin
//1640570

package inheritance;

public class Book {
protected String title;
private String author;

public String getTitle() {
	return this.title;
}

public String getAuthor() {
	return this.author;
}

public Book(String title, String author) {
	this.title = title;
	this.author = author;
}

public String toString() {
	return("title: " + title + ", " + "author: " + author);
}
}


