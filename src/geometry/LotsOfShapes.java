//Leon Hazurin
//1640570

package geometry;

public class LotsOfShapes {
public static void main(String[] args) {
	Shape[] shapes = new Shape[5];
	
	shapes[0] = new Circle(3);
	shapes[1] = new Circle(4);
	shapes[2] = new Rectangle(2,4);
	shapes[3] = new Rectangle(3,4);
	shapes[4] = new Square(5);
	
	for(int i = 0; i < shapes.length; i++) {
		System.out.println("Area: " + shapes[i].getArea() + " Perimeter: " + shapes[i].getPerimeter());
		
	}
}
}
