//Leon Hazurin
//1640570

package geometry;

public interface Shape {
double getArea();
double getPerimeter(); 
}
