//Leon Hazurin
//1640570

package geometry;

public class Rectangle implements Shape {
private double length;
private double width;


public Rectangle(double length, double width) {
	this.length = length;
	this.width = width;
}

public double getLength() {
	return this.length;
}

public double getWidth() {
	return this.width;
}

public double getArea() {
	return width*length;
}

public double getPerimeter() {
	return (2*width)+(2*length);
}
}
